Oil lamps
=========

Oil lamp mod for Minetest

Create (quite simple) oil lamps for illumination.

Steps to create oil lamps:
1. craft raw enclosures from clay (8 clay lumps for 8 raw enclosures)
1. burn raw enclosures to empty enclosures
1. fill empty empty enclosures with oil extract/palm wax/parafin to full oil lamps

Additional source of oil extract:
* craft oil extract out of any seeds


Author: ademant
