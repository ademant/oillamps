
local S = minetest.get_translator("default")
minetest.register_craftitem("oillamps:oillampraw", {
	image = "oil_lamp_raw_enclosure.png",
    description=S("Raw oil lamps"),
	short_description=S("Raw oil lamps"),
})

minetest.register_craftitem("oillamps:oillampempty", {
	image = "oil_lamp_empty.png",
    description=S("empty oil lamps"),
	short_description=S("empty oil lamps"),
})
