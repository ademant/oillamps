local S = minetest.get_translator("default")
minetest.register_node("oillamps:oillamp", {
    description = S("Oil lamp"),
    tiles = {{
            name = "oil_lamp.png",
            animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 12}
    }},
    paramtype2 = "facedir",
    place_param2 = 0,
    is_ground_content = false,
    stack_max= 1,
    groups = {cracky = 3},
    light_source = 14,
    on_blast = function() end,
    sounds = default.node_sound_stone_defaults(),
})
