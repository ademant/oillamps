
oillamps.burn_resource = {"basic_materials:oil_extract","ethereal:palm_wax","basic_materials:paraffin"}
oillamps.bexist = false
oillamps.existing_ressource = {}

-- check that at least one burning ressource exist
for _,resource in pairs(oillamps.burn_resource) do
	oillamps.bexist = oillamps.bexist or (minetest.registered_items[resource] ~= nil)
	if (minetest.registered_items[resource]) then
		table.insert(oillamps.existing_ressource,resource)
	end
end
