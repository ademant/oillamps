
oillamps = {
mod = {author = "Andreas Demant"},
modpath = minetest.get_modpath("oillamps"),
modname = minetest.get_current_modname(),
}

minetest.log("action", "[MOD]"..oillamps.modname.." -- start loading ")

dofile(oillamps.modpath .. "/config.lua")
if(oillamps.bexist) then
	dofile(oillamps.modpath .. "/nodes.lua")
	dofile(oillamps.modpath .. "/craftitems.lua")
	dofile(oillamps.modpath .. "/crafts.lua")
else
	minetest.log("action", "[MOD]"..oillamps.modname.." -- no ressources found -> not defining oil lamps ")
end

minetest.log("action", "[MOD]"..oillamps.modname.." -- end loading ")

