
-- craft raw oil lamp enclosures by using 8 clump of clay
-- this raw enclosure has to be burned to be stable
minetest.register_craft({
	output = "oillamps:oillampraw 8",
	recipe = {{"default:clay_lump","default:clay_lump","default:clay_lump"},{"default:clay_lump","","default:clay_lump"},{"default:clay_lump","default:clay_lump","default:clay_lump"}},
})

-- create a usable oil lamp by burning raw enclosures
minetest.register_craft({
	output = "oillamps:oillampempty 1",
	type = "cooking",
	recipe = "oillamps:oillampraw",
	cooktime = 3
})

if minetest.registered_items["basic_materials:oil_extract"] then
	minetest.register_craft({
		output = "basic_materials:oil_extract 2",
		recipe = {{"group:seed","group:seed"},{"group:seed","group:seed"}},
	})
end

-- fill an empty oil lamp with oil extract
for _,resource in pairs(oillamps.existing_ressource) do
	if minetest.registered_items[resource] then
		minetest.register_craft({
			recipe = {{resource},{"oillamps:oillampempty"}},
			output = "oillamps:oillamp 1",
		})
	end
end
